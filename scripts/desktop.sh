#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

# force accept config options
echo "Dpkg::Options {
  \"--force-confdef\";
  \"--force-confold\";
}" >> /etc/apt/apt.conf.d/local

# Install ubuntu unity
apt-get autoremove -y
apt-get upgrade -y
tasksel install ubuntu-desktop

# Disable start menu animations
gsettings set org.gnome.desktop.interface enable-animations false

#Disable the "welcome" screen in gnome - Throwing a bunch of stuff at the wall here
# Since gnome isn't great at obeying settings
mkdir -p /home/vagrant/.config
echo "yes" >> /home/vagrant/.config/gnome-initial-config-done

# Fix permissions on that vagrant folder now.
chown -R vagrant:vagrant /home/vagrant

# Remove the initial setup tool completely.
apt-get remove -y --auto-remove gnome-initial-setup 
