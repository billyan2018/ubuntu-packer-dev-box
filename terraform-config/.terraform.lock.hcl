# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version = "2.10.0"
  hashes = [
    "h1:q4kR5osLdX2Eo2XLb786+EkcM1nZQv3i/nAK/L/icg4=",
    "zh:0140f0951ad1f2005498ea33ee77c5ba477a14769ee6af9e75ce988db7b394c4",
    "zh:01e534208c690f658e40312c4eae8053f109d003502d8ecc686524e13e387450",
    "zh:0ecbc0bb323134603694c1e21942c611a1c21a7d26d99790c6b312aec8e382b6",
    "zh:44ec2044b94209a9e913ef7816709efad865138329f4b9dfa4b12234267c9e16",
    "zh:6ddca59bf4975b4c62969cb60ed5c4520a8ab000e45bba87f388bdc3e98680c4",
    "zh:851c048e643924fd8a5563ffd9d557e32dd3b5d1256e4a0e359a7ff1e15fd705",
    "zh:b05030578016d6a33d5038fd54c71d9c7d0ffeee49595d4aebb5e85c6d1bef31",
    "zh:b14a74ec3f7889b0757a273c20b99e731068fb9b75dd4f5676bb42ee0a256a8c",
    "zh:bde356436036e7f6f581df241721e91f01f17b8f911441a8c04ff22e3a7a1321",
    "zh:c59e345af47c4e7874f690a8413cd028b34dc30e7711def1e913860a8d49fe9b",
    "zh:c78b2a7285ecb0a530a536765f2f9c306138c4b1305a773cc4a6e62ad1dda15d",
    "zh:ed1e593186686d415cccd50855662f436e7114c2fa0d4fb3b9fe00fa6c1c7ff6",
  ]
}
