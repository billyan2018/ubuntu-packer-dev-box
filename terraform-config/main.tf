terraform {
  backend "gcs" {
    bucket  = "ubuntu-packer-248022-remote-state"
    prefix  = "terraform/packer-state"
	credentials = "ubuntu-packer.json"
  }
  
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {
  project = "ubuntu-packer-248022"
  region  = var.region
  zone    = "us-central1-c"
  credentials = file("ubuntu-packer.json")
}

resource "google_compute_disk" "nested_virtualize_disk" {
  name  = "${var.instance-name}-disk"
  type  = "pd-ssd"
  zone  = var.zone
  image = "ubuntu-2004-focal-v20210720"

  timeouts {
	create = "60m"
  }
}

resource "google_compute_image" "nested_virtualize_image" {
  name = "${var.instance-name}-disk"
  source_disk = google_compute_disk.nested_virtualize_disk.self_link
  licenses = [
    "https://www.googleapis.com/compute/v1/projects/vm-options/global/licenses/enable-vmx",
  ]
  timeouts {
	create = "60m"
  }
}



resource "google_compute_instance" "gcp_instance" {
  name = var.instance-name
  machine_type = var.vm_type

  zone = var.zone

  min_cpu_platform = "Intel Haswell"

  tags = [
    "${var.network}-firewall-ssh",
    "${var.network}-firewall-http",
    "${var.network}-firewall-https",
    "${var.network}-firewall-icmp",
    "${var.network}-firewall-openshift-console",
    "${var.network}-firewall-secure-forward",
  ]

  boot_disk {
    initialize_params {
      image = google_compute_image.nested_virtualize_image.self_link
      type  = "pd-ssd"
      size  = var.disk_size
    }
  }

  metadata = {
    hostname = var.instance-name
	sshKeys  = "${var.ssh_username}:${file(var.ssh_username_public_key)}"
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet.name
    
    access_config {
      // Ephemeral IP
    }
  }

  scheduling {
    preemptible = "false"
    automatic_restart = "false"
  }
  
  timeouts {
	create = "60m"
  }
}
